package com.dfp.persistencia.entities;


import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by Alberto on 30/12/2015.
 */
public final class EMF {
    private static final EntityManagerFactory emfInstance =
            Persistence.createEntityManagerFactory("transactions-optional");

    private EMF() {
    }

    public static EntityManagerFactory get() {
        return emfInstance;
    }
}
