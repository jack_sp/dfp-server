package com.dfp.persistencia.entities;

import com.google.appengine.api.datastore.Key;

import javax.persistence.*;

/**
 * Created by Alberto on 30/12/2015.
 */
@Entity
public class Aeropuerto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Key key;


    @OneToOne(mappedBy = "aeropuerto")
    private Vuelo vuelo;
    

    private String nombreAeropuerto = "";

    private String codigoAeropuerto = "";



    public String getNombreAeropuerto() {
		return nombreAeropuerto;
	}

	public void setNombreAeropuerto(String nombreAeropuerto) {
		this.nombreAeropuerto = nombreAeropuerto;
	}

	public String getCodigoAeropuerto() {
		return codigoAeropuerto;
	}

	public void setCodigoAeropuerto(String codigoAeropuerto) {
		this.codigoAeropuerto = codigoAeropuerto;
	}

	public Key getKey() {
        return key;
    }

    
    
}
