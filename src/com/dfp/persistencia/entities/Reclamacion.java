package com.dfp.persistencia.entities;

import com.dfp.core.dto.ReclamacionDTO;
import com.google.appengine.api.datastore.Key;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Alberto on 30/12/2015.
 */
@Entity
public class Reclamacion {

	// propiedades
	@Id // id de la entidad
	@GeneratedValue(strategy = GenerationType.IDENTITY) // el proovedor de
														// persistencia gnerar
														// un valor al
														// insertarlo y se lo da
	private Key key;

	private String codigoReclamacion = "";

	private String idPasajero = "";

	private String textoReclamacion = "";

	private Date fechaReclamacion = null;

	private Date fechaVuelo = null;

	private Date horaInicioVueloReclamacion = null;

	private Date horaFinVueloReclamacion = null;

	@OneToOne(cascade=CascadeType.ALL)
	private Estado estado = null;

	// geters y setters

	

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Date getFechaReclamacion() {
		return fechaReclamacion;
	}

	public void setFechaReclamacion(Date fechaReclamacion) {
		this.fechaReclamacion = fechaReclamacion;
	}

	public Date getFechaVuelo() {
		return fechaVuelo;
	}

	public void setFechaVuelo(Date fechaVuelo) {
		this.fechaVuelo = fechaVuelo;
	}

	// private Vuelo parentVuelo = null;

	public Date getHoraInicioVueloReclamacion() {
		return horaInicioVueloReclamacion;
	}

	public void setHoraInicioVueloReclamacion(Date horaInicioVueloReclamacion) {
		this.horaInicioVueloReclamacion = horaInicioVueloReclamacion;
	}

	public Date getHoraFinVueloReclamacion() {
		return horaFinVueloReclamacion;
	}

	public void setHoraFinVueloReclamacion(Date horaFinVueloReclamacion) {
		this.horaFinVueloReclamacion = horaFinVueloReclamacion;
	}

	@ElementCollection
	private List<String> idCasosUso = null;

	// En la Entidad Vuelo se referencia una instancia simple de la Entidad
	// Pasajero.
	/*
	 * @ManyToOne(optional = true, fetch = FetchType.EAGER, cascade =
	 * CascadeType.ALL) public Vuelo getParentVuelo() { return parentVuelo; }
	 * 
	 * public void setParentVuelo(final Vuelo parentVuelo) { this.parentVuelo =
	 * parentVuelo; }
	 */

	private Pasajero parentPasajero = null;

	// En la Entidad Vuelo se referencia una instancia simple de la Entidad
	// Pasajero.
	@ManyToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	public Pasajero getParentPasajero() {
		return parentPasajero;
	}

	public void setParentPasajero(final Pasajero parentPasajero) {
		this.parentPasajero = parentPasajero;
	}

	/*
	 * private Estado parentEstado = null;
	 * 
	 * // En la Entidad Vuelo se referencia una instancia simple de la Entidad
	 * Pasajero.
	 * 
	 * @ManyToOne(optional = true, fetch = FetchType.EAGER, cascade =
	 * CascadeType.ALL) public Estado getParentEstado() { return parentEstado; }
	 * 
	 * public void setParentEstado(final Estado parentEstado) {
	 * this.parentEstado = parentEstado; }
	 */

	@ElementCollection
	private List<String> documentosAnexos = null;

	@Transient
	private Double importeReclamacion = null;

	public Double getImporteReclamacion() {
		return importeReclamacion;
	}

	public void setImporteReclamacion(Double importeReclamacion) {
		this.importeReclamacion = importeReclamacion;
	}

	public List getDocumentosAnexos() {
		return documentosAnexos;
	}

	public void setDocumentosAnexos(List documentosAnexos) {
		this.documentosAnexos = documentosAnexos;
	}

	public String getCodigoReclamacion() {
		return codigoReclamacion;
	}

	public void setCodigoReclamacion(String codigoReclamacion) {
		this.codigoReclamacion = codigoReclamacion;
	}

	public String getIdPasajero() {
		return idPasajero;
	}

	public void setIdPasajero(String idPasajero) {
		this.idPasajero = idPasajero;
	}

	public List getIdCasosUso() {
		return idCasosUso;
	}

	public void setIdCasosUso(List idCasosUso) {
		this.idCasosUso = idCasosUso;
	}

	public String getTextoReclamacion() {
		return textoReclamacion;
	}

	public void setTextoReclamacion(String textoReclamacion) {
		this.textoReclamacion = textoReclamacion;
	}

	// populate
	public void populateFromReclamacionDTO(ReclamacionDTO reclamacion) {
		if (reclamacion.getCodigoReclamacion() != null)
			this.setCodigoReclamacion(reclamacion.getCodigoReclamacion());
		if (reclamacion.getFechaVuelo() != null)
			this.setFechaVuelo(reclamacion.getFechaVuelo());
		if (reclamacion.getTextoReclamacion() != null)
			this.setTextoReclamacion(reclamacion.getTextoReclamacion());
		if (reclamacion.getDocumentosAnexos() != null)
			this.setDocumentosAnexos(reclamacion.getDocumentosAnexos());

		EntityManager em = EMF.get().createEntityManager();

		if (reclamacion.getEstadoDTO() != null) {
			Query q = em.createQuery(
					"select e from Estado e where  e.secEstado =" + reclamacion.getEstadoDTO().getSecEstado());
			// estado = (String) (new ArrayList(q.getResultList())).get(0);
			Estado estado = (Estado) (new ArrayList(q.getResultList())).get(0);
			if (reclamacion.getEstadoDTO().getSecEstado() != null) {
				this.setEstado(estado);
			} else {
				estado.setSecEstado(0);
				this.setEstado(estado);
			}
		}

	}
}
