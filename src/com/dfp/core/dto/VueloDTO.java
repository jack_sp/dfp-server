package com.dfp.core.dto;

import java.util.Date;

/**
 * Created by Alberto on 30/12/2015.
 */
public class VueloDTO {


    private String codigoVuelo = "";

    private String idCodigoCompañia = "";

    private Date horaInicioVuelo = null;

    private Date horaFinVuelo = null;

    public String getCodigoVuelo() {
        return codigoVuelo;
    }

    public void setCodigoVuelo(String codigoVuelo) {
        this.codigoVuelo = codigoVuelo;
    }

    public String getIdCodigoCompañia() {
        return idCodigoCompañia;
    }

    public void setIdCodigoCompañia(String idCodigoCompañia) {
        this.idCodigoCompañia = idCodigoCompañia;
    }

    public Date getHoraInicioVuelo() {
        return horaInicioVuelo;
    }

    public void setHoraInicioVuelo(Date horaInicioVuelo) {
        this.horaInicioVuelo = horaInicioVuelo;
    }

    public Date getHoraFinVuelo() {
        return horaFinVuelo;
    }

    public void setHoraFinVuelo(Date horaFinVuelo) {
        this.horaFinVuelo = horaFinVuelo;
    }


}
