package com.dfp.services.internal;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.google.appengine.api.datastore.Blob;
import com.dfp.core.dto.ReclamacionDTO;
import com.dfp.dao.ReclamacionDao;
import com.dfp.services.ReclamacionCxfRestService;

public class ReclamacionCxfRestServiceImpl implements ReclamacionCxfRestService {
	
	private static final Logger log = Logger.getLogger(ReclamacionCxfRestServiceImpl.class.getName());

	@Autowired
	private ReclamacionDao reclamacionDao;

	@Override
	public Response getClaimDetail(String claimId) {
		return null;
	}

	@Override
	public Response listClaims() {
		List<ReclamacionDTO> oListReclamacion = new ArrayList<ReclamacionDTO>();

		return Response.ok(oListReclamacion).build();
	}

	@Override
	public Response insertClaim(ReclamacionDTO claimDTO) {
		return Response.ok(reclamacionDao.insertClaim(claimDTO)).build();
	}

	@Override
	@POST
	public String enviaMail(@Context HttpServletRequest request) {
		ServletFileUpload upload = new ServletFileUpload();
		String sResultado = "";
		log.info("Entrada de un email");
		System.out.println("Entrada de un email");
		// log.debug("Entrada de un email"+request.getParameter("photo"));

		try {
			FileItemIterator fileIterator = upload.getItemIterator(request);
			String email = "";
			String msgBody = "";
			String textarea = "";
			String telf = "";
			String calendario = "";
			byte[] content = null;

			MailUtil oMailUtil = new MailUtil();

			while (fileIterator.hasNext()) {

				FileItemStream item = fileIterator.next();
				/*
				 * if ("uploadedFile".equals(item.getFieldName())){ byte[]
				 * content = IOUtils.toByteArray(item.openStream()); } else if
				 * ("email".equals(item.getFieldName())){ email =
				 * IOUtils.toString(item.openStream()); msgBody = msgBody +
				 * "\n Email:\n"+email; }else if
				 * ("foto".equals(item.getFieldName())){ byte[] content =
				 * IOUtils.toByteArray(item.openStream()); }else if
				 * ("calendario".equals(item.getFieldName())){ String
				 * calendario=IOUtils.toString(item.openStream()); msgBody =
				 * msgBody + "\n Calendario:\n"+calendario; }else if
				 * ("textarea-1".equals(item.getFieldName())){ String
				 * textarea=IOUtils.toString(item.openStream()); msgBody =
				 * msgBody + "\n Observaciones:\n"+textarea; }
				 */
				if ("file".equals(item.getFieldName())) {
					System.out.println("uploadedFile leido");
					content = IOUtils.toByteArray(item.openStream());
					System.out.println("uploadedFile leido" + content);
					// Save content into datastore
					// ...
				} else if ("email".equals(item.getFieldName())) {
					email = IOUtils.toString(item.openStream());
					System.out.println("email leido" + email);
					// Do something with the name string
					// ...
				} else if ("textarea".equals(item.getFieldName())) {
					textarea = IOUtils.toString(item.openStream());

				} else if ("calendario".equals(item.getFieldName())) {
					calendario = IOUtils.toString(item.openStream());
				} else if ("telf".equals(item.getFieldName())) {
					telf = IOUtils.toString(item.openStream());
				}
			}

			// construct our entity objects
			Blob imageBlob = new Blob(content);
			String sTimeStamp = email + getTimeStamp();
			PresupuestoImagenVO myImage = new PresupuestoImagenVO(sTimeStamp, imageBlob);

			PersistenceManager pm = PMF.get().getPersistenceManager();
			pm.makePersistent(myImage);
			pm.close();

			String sEmailPeticionPresupuesto = TextoEmail.textoCorreoPresupuesto;
			sEmailPeticionPresupuesto = sEmailPeticionPresupuesto.replace("###MSGBODY###",
					email + " <br> " + telf + " <br>  " + calendario + " <br> Observaciones:  <br> " + textarea
							+ "<br><a href=\"http://multasradar4.appspot.com/upload?email=" + sTimeStamp
							+ "\" >Ver multa foto</a>");
			sResultado = oMailUtil.envioCorreo1(email, sEmailPeticionPresupuesto);

		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sResultado;
	}

}
