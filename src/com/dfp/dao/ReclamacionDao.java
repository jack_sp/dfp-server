package com.dfp.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.dfp.core.EMFService;
import com.dfp.core.dto.ReclamacionDTO;
import com.dfp.persistencia.entities.Reclamacion;

public class ReclamacionDao {
	
	/**
	 * devuelve la lista de reclamaciones
	 */
	public List<Reclamacion> listClaims(String sCodeClaim) {
		EntityManager em = EMFService.get().createEntityManager();
		List<Reclamacion> oListReclamacion = null;
		String sQuery = "select r from Reclamacion r ";
		
		if(!sCodeClaim.isEmpty())
			sQuery = sQuery + " where r.codigoReclamacion = "+sCodeClaim;
			
		try {
			Query q = em.createQuery(sQuery);
			oListReclamacion = new ArrayList(q.getResultList());
		} catch (Exception e) {
			System.out.println("Errro al recuperar la lista de reclamaciones");
		} finally {
			em.close();
			return oListReclamacion;
		}
	}
	
	/**
	 * devuelve la lista de reclamaciones
	 */
	public List<Reclamacion> listClaims() {
		return this.listClaims("");
	}

	/**
	 * @param claimDTO, inserta reclamacion en datastore
	 * @return
	 */
	public int insertClaim(ReclamacionDTO claimDTO) {
		List<Reclamacion> oListReclamacion = null;
		EntityManager em = EMFService.get().createEntityManager();
		int indexClaim = 0;
		
		try {
			Query q = em.createQuery("select r from Reclamacion r ORDER BY r.codigoReclamacion DESC");
			oListReclamacion = new ArrayList(q.getResultList());
			if (oListReclamacion == null)
				indexClaim = 0;
			else{
				indexClaim = new Integer(oListReclamacion.get(0).getCodigoReclamacion());
			}
			Reclamacion oReclamacion = new Reclamacion();
			claimDTO.setCodigoReclamacion(""+indexClaim+1);
			
			oReclamacion.populateFromReclamacionDTO(claimDTO);			
			em.persist(oReclamacion);
		} catch (Exception e) {
			System.out.println("Errro al recuperar la lista de reclamaciones");
			indexClaim = -1;
		} finally {
			em.close();
			return indexClaim;
		}
	}
	
	
	/**
	 * acceso a datos según el código de la reclamacion
	 * 
	 * @param employeeLastName
	 * @return
	 */
	public List<Reclamacion> getClaimDetail(String claimCode) {
		List<Reclamacion> oListReclamacion = this.listClaims(claimCode);
//		int index = 0;
//		while (index < oListReclamacion.size()
//				&& !oListReclamacion.get(index).getCodigoReclamacion().equals(claimCode)) {
//			index++;
//		}

//		if (index < oListReclamacion.size())
//			return ((Reclamacion) oListReclamacion.get(index));
//		else
//			return null;
		return oListReclamacion;
	}
}
