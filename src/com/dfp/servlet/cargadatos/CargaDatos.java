package com.dfp.servlet.cargadatos;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.dfp.persistencia.entities.Aeropuerto;
import com.dfp.persistencia.entities.Estado;
import com.dfp.persistencia.entities.Nacion;
import com.prueba.cxfrestservice.model.Employee;

public class CargaDatos {
	
	
	//Delimitador de las filas del csv
    private static final String NEW_LINE_SEPARATOR = "\n";

    //Atributos de cada empleado
    private static final String employeeFormat[] = {"FIRST_NAME","LAST_NAME","BIRTH_DATE","POSITION"};
    
    private static final String paisesFormat[] = {"nombre", "name", "nom", "iso2", "iso3", "phone_code"};
    
    private static final String aeropuertoFormat[] = {"nombre","codigo"};
    
    private static final String estadoFormat[] = {"Nombre_estado","sec_estado","descripcion_estado"};
    
    private static  CSVParser parser = null;
    
	public static CSVParser getParser() {
		return parser;
	}

	public static void setParser(CSVParser parser) {
		CargaDatos.parser = parser;
	}

	public static void getCSVParser(ServletContext context,String sUrl) throws FileNotFoundException, IOException{
//    	Carga así para poder apromiarse de los permisos de lectura del fichero
		//dentro del servidor además de proporcionar una ruta relativa 
		InputStream is = context.getResourceAsStream(sUrl);
		
		Reader reader = new InputStreamReader(is, "UTF-8");
    	
        //Crear el CSVFormat object
    	 CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');
         CSVParser parser = new CSVParser(reader, format);
         setParser(parser);
	}



    public static  List<Employee> importData(ServletContext context) throws FileNotFoundException, IOException {
    	
    	
    	CSVParser parser = getParser();
    	//
         List<Employee> emps = new ArrayList<Employee>();
         for(CSVRecord record : parser){
             Employee emp = new Employee();
             emp.setFirstName(record.get(employeeFormat[0]));
             emp.setLastName(record.get(employeeFormat[1]));
             emp.setDateBirth(record.get(employeeFormat[2]));
             emp.setPosition(record.get(employeeFormat[3]));
             emps.add(emp);
         }
         parser.close();
         
        return emps;
    }
    
    public static  List<Nacion> importPaises(ServletContext context) throws FileNotFoundException, IOException {
    	CSVParser parser = getParser();
    	//
         List<Nacion> naciones = new ArrayList<Nacion>();
         for(CSVRecord record : parser){
             Nacion nacion = new Nacion();
             nacion.setNombreNacion(record.get(paisesFormat[0]));
             nacion.setCodigoNacion(record.get(paisesFormat[4]));
             nacion.setCodigoPhone(record.get(paisesFormat[5]));
             naciones.add(nacion);
         }
         parser.close();
         
        return naciones;
    }

	public static List<Aeropuerto> importAeropuertos(ServletContext context) throws FileNotFoundException, IOException {
	 	CSVParser parser = getParser();
    	//
         List<Aeropuerto> aeropuertos = new ArrayList<Aeropuerto>();
         for(CSVRecord record : parser){
        	 Aeropuerto aeropuerto = new Aeropuerto();
        	 aeropuerto.setNombreAeropuerto(record.get(aeropuertoFormat[0]));
        	 aeropuerto.setCodigoAeropuerto(record.get(aeropuertoFormat[1]));
             aeropuertos.add(aeropuerto);
         }
         parser.close();
         
        return aeropuertos;
	}

	public static List<Estado> importEstados(ServletContext context) throws FileNotFoundException, IOException {
		CSVParser parser = getParser();
    	//
         List<Estado> estados = new ArrayList<Estado>();
         for(CSVRecord record : parser){
        	 Estado estado = new Estado();
        	 
        	 estado.setNombreEstado(record.get(estadoFormat[0]));
        	 estado.setSecEstado(new Integer(record.get(estadoFormat[1])));
        	 estado.setDescripcionEstado(record.get(estadoFormat[2]));
        	 
             estados.add(estado);
         }
         parser.close();
         
        return estados;
	}


}
