package com.dfp.servlet.cargadatos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CargaDatosServlet  extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		ServletContext context = getServletContext();
		
		String sUrl = "/WEB-INF/datos/employees.csv";
		CargaDatos.getCSVParser(context, sUrl);
		CargaDatos.importData(context);
		
		sUrl = "/WEB-INF/datos/paises.csv";
		CargaDatos.getCSVParser(context, sUrl);
		CargaDatos.importPaises(context);

		sUrl = "/WEB-INF/datos/aeropuertos.csv";
		CargaDatos.getCSVParser(context, sUrl);
		CargaDatos.importAeropuertos(context);
		
		sUrl = "/WEB-INF/datos/estados.csv";
		CargaDatos.getCSVParser(context, sUrl);
		CargaDatos.importEstados(context);
		
	}
	

}

