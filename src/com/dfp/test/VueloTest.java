package com.dfp.test;

import com.dfp.persistencia.entities.DireccionPasajero;
import com.dfp.persistencia.entities.Pasajero;
import com.dfp.persistencia.entities.Vuelo;
import com.dfp.core.EMFService;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by amfranco on 15/01/2016.
 */
public class VueloTest {

    public List<Vuelo> testPasajeros(){
        EntityManager em = EMFService.get().createEntityManager();
        int error = 0;
        Vuelo vuelo = new Vuelo();
        vuelo.setCodigoVuelo("BA2490");

        Pasajero pasajero = new Pasajero();
        pasajero.setNombre("Carli");

        DireccionPasajero direccionPasajero = new DireccionPasajero();
        direccionPasajero.setLocalidad("Oviedo");
        direccionPasajero.setCalle("Rector Leopoldo");

        //pasajero.setDireccionPasajero(direccionPasajero);

        Pasajero pasajero1 = new Pasajero();
        pasajero1.setNombre("Carla");

        DireccionPasajero direccionPasajero1 = new DireccionPasajero();
        direccionPasajero1.setLocalidad("Gijón ");
        direccionPasajero1.setCalle("calle ancha");

        //pasajero1.setDireccionPasajero(direccionPasajero1);

        List<Pasajero> pasajeros = new ArrayList<Pasajero>();
        pasajeros.add(pasajero);
        pasajeros.add(pasajero1);

        List<Vuelo> vuelos = new ArrayList<Vuelo>();
        vuelo.setPasajeros(pasajeros);

        try {
            em.persist(vuelo);
            //em.persist(direccionPasajero);
            Query q = em.createQuery("select v from Vuelo v");

            vuelos = new ArrayList(q.getResultList());
        }catch(Exception e){
            vuelos = null;
        }finally {
            em.close();
            return vuelos;
        }
    }



}
