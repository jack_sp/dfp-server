package com.dfp.test;

import com.dfp.persistencia.entities.*;
import com.dfp.core.EMFService;

import javax.persistence.EntityManager;

/**
 * Created by amfranco on 04/02/2016.
 */
public class ReclamacionTest {

    public  int testReclamacion(){
        EntityManager em = EMFService.get().createEntityManager();
        int error = 0;
        Reclamacion reclamacion = new Reclamacion();
        Vuelo vuelo = new Vuelo();
        Compania compania = new Compania();
        compania.setNombreCompania("Air Italia");


        Pasajero oPasajero = new Pasajero();
        oPasajero.setNombre("C. Marqués");

        Nacion oNacionPasajero = new Nacion();
        oNacionPasajero.setNombreNacion("España");

        //direccion pasajero
        DireccionPasajero oDireccionPasajero = new DireccionPasajero();
        oDireccionPasajero.setCalle("calle.... ");
        oDireccionPasajero.setProvincia("Roma");
        oDireccionPasajero.setNacion(oNacionPasajero);
        oPasajero.setDireccionPasajero(oDireccionPasajero);

        //direccion compañia
        DireccionCompania oDireccionCompania = new DireccionCompania();
        oDireccionCompania.setCalle("calle de air italia ");
        oDireccionCompania.setProvincia("Roma, Italia");


        Nacion oNacionCompania = new Nacion();
        oNacionPasajero.setNombreNacion("Italia");

        oDireccionCompania.setNacion(oNacionCompania);




        compania.setDireccionCompania(oDireccionCompania);
       // Nacion oNacionCompania = new Nacion();
      //  oNacionCompania.setNombreNacion("Alemania");

     //   oDireccionCompania.setNacion(oNacionCompania);
        //compania.setDireccionPasajero(oDireccionCompania);



        vuelo.setCodigoVuelo("VUL-12345");
        vuelo.setCompania(compania);
        oPasajero.setParentVuelo(vuelo);
        reclamacion.setParentPasajero(oPasajero);

        try {
            em.persist(reclamacion);
            //em.persist(direccion);
            /*Query q = em.createQuery(
                    "select p from Pasajero p where p.nombre  = :p_nombre ");
            q.setParameter("p_nombre", sNNombrePasajero);

            List<Pasajero> pjs = q.getResultList();

            System.out.println("recupoerados " +pjs.size()+" pasajeros");*/
        }catch(Exception e){
            error = 1;
        }finally {
            em.close();
            return error;
        }
    }




}
