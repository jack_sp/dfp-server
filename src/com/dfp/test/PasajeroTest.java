package com.dfp.test;

import com.dfp.persistencia.entities.DireccionPasajero;
import com.dfp.persistencia.entities.Pasajero;
import com.dfp.core.EMFService;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alberto on 01/01/2016.
 */
public class PasajeroTest {

    public int testPasajero(){
        EntityManager em = EMFService.get().createEntityManager();
        int error = 0;
        Pasajero pasajero = new Pasajero();
        DireccionPasajero direccionPasajero = new DireccionPasajero();

        direccionPasajero.setLocalidad("Oviedo");
        direccionPasajero.setCalle("Rector Leopoldon 10");
        List<Pasajero> pasajeros = null;
        pasajero.setNombre("Carli");
        //pasajero.setDireccionPasajero(direccionPasajero);

        try {
            em.persist(pasajero);
            em.persist(direccionPasajero);
            Query q = em.createQuery(
                    "select p from Pasajero p where p.nombre='Carli' ");
            pasajeros = new ArrayList(q.getResultList());
        }catch(Exception e){
            error = 1;
        }finally {
            em.close();
            return error;
        }
    }

   /* public static void main(String[] args) {

        EntityManager em = EMFService.get().createEntityManager();
        Pasajero pasajero = new Pasajero();
        List<Pasajero> pasajeros = null;
        pasajero.setNombre("Carla");

        try {
            em.persist(pasajero);
            Query q = em.createQuery("select p from Pasajero p");
            pasajeros = new ArrayList(q.getResultList());
        } finally {
            em.close();
        }

      *//*  EntityManagerFactory emf =
                Persistence.createEntityManagerFactory("transactions-optional");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Pasajero pasajero = new Pasajero();

        pasajero.setNombre("Carla");

        tx.begin();
        try {
            em.persist(pasajero);
            tx.commit();
        } catch(Exception e) {
            tx.rollback();
        }

        em.close();
        emf.close();*//*

    }*/

}