var app = angular.module("app", []);

function RemoteResource($http, baseUrl,$location) {
  this.get = function(fnOK, fnError) {
    $http({
      method: 'GET',
      url: baseUrl+'/CxfRestService/rest/employeeservices/getemployeedetail',
      params: {employeeId: ($location.search()).employeeId}
    }).success(function(data, status, headers, config) {
      fnOK(data);
    }).error(function(data, status, headers, config) {
      fnError(data, status);
    });
  }
  this.list = function(fnOK, fnError) {
    $http({
      method: 'GET',
     url:  baseUrl+'/CxfRestService/rest/employeeservices/listEmployees'
    }).success(function(data, status, headers, config) {
      fnOK(data);
    }).error(function(data, status, headers, config) {
      fnError(data, status);
    });
  }
}

function RemoteResourceProvider() {
  var _baseUrl;
  this.setBaseUrl = function(baseUrl) {
    _baseUrl = baseUrl;
  }
  this.$get = ['$http','$location',
    function($http,$location) {
      return new RemoteResource($http, '.',$location);
    }
  ];
}

app.provider("remoteResource", RemoteResourceProvider);


app.constant("baseUrl", ".");
app.config(['baseUrl','$locationProvider', 'remoteResourceProvider',
  function(baseUrl , $locationProvider, remoteResourceProvider) {
	$locationProvider.html5Mode(true);    
    remoteResourceProvider.setBaseUrl(baseUrl);
  }
]);



app.controller("DetalleSeguroController", ['$scope', 'remoteResource','$location',function($scope, remoteResource,$location) {
    remoteResource.get(function(empleado) {
        $scope.empleado = empleado;
    }, function(data, status) {
      alert("Ha fallado la petición. Estado HTTP:" + status);
    });

}]);

app.controller("ListadoSeguroController", ['$scope', 'remoteResource',function($scope, remoteResource) {
    $scope.empleados = [];

    remoteResource.list(function(empleados) {
      $scope.empleados = empleados;
    }, function(data, status) {
      alert("Ha fallado la petición. Estado HTTP:" + status);
    });

}]);

app.controller("MainController", ['$scope',function($scope) {

}]);